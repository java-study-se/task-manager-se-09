package com.morozov.tm.command;

import com.morozov.tm.api.IServiceLocator;
import com.morozov.tm.exception.RepositoryEmptyException;
import com.morozov.tm.exception.StringEmptyException;
import com.morozov.tm.exception.UserExistException;
import com.morozov.tm.exception.UserNotFoundException;
import com.morozov.tm.enumerated.UserRoleEnum;
import org.jetbrains.annotations.NotNull;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;


public abstract class AbstractCommand {

    protected IServiceLocator serviceLocator;
    protected List<UserRoleEnum> userRoleList = new ArrayList<>();

    public void setServiceLocator(@NotNull IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public abstract String getName();

    public abstract String getDescription();

    public abstract void execute() throws ParseException, StringEmptyException, RepositoryEmptyException, UserNotFoundException, UserExistException;

    public List<UserRoleEnum> getUserRoleList() {
        return userRoleList;
    }
}
