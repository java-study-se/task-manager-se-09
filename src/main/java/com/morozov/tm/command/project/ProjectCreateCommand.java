package com.morozov.tm.command.project;

import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.entity.Project;
import com.morozov.tm.exception.StringEmptyException;
import com.morozov.tm.entity.User;
import com.morozov.tm.enumerated.UserRoleEnum;
import com.morozov.tm.util.ConsoleHelperUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.text.ParseException;

public class ProjectCreateCommand extends AbstractCommand {

    public ProjectCreateCommand() {
        userRoleList.add(UserRoleEnum.ADMIN);
        userRoleList.add(UserRoleEnum.USER);
    }

    @NotNull
    @Override
    final public String getName() {
        return "project-create";
    }

    @NotNull
    @Override
    final public String getDescription() {
        return "Create project";
    }

    @Override
    final public void execute() throws StringEmptyException {
        ConsoleHelperUtil.writeString("Введите имя нового проекта");
        @NotNull final String projectName = ConsoleHelperUtil.readString();
        @Nullable final User currentUser = serviceLocator.getCurrentUser();
        if (currentUser != null) {
            @NotNull final Project addedProject = serviceLocator.getProjectService().addProject(currentUser.getId(), projectName);
            ConsoleHelperUtil.writeString("Добавлен проект: " + addedProject.getName());
        } else {
            ConsoleHelperUtil.writeString("Текущий пользователь не установлен");
        }
    }
}
