package com.morozov.tm.command.project;

import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.entity.User;
import com.morozov.tm.enumerated.UserRoleEnum;
import com.morozov.tm.util.ConsoleHelperUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;



public class ProjectRemoveAllCommand extends AbstractCommand {
    public ProjectRemoveAllCommand() {
        userRoleList.add(UserRoleEnum.USER);
        userRoleList.add(UserRoleEnum.ADMIN);
    }

    @Override
    public String getName() {
        return "project-removeall";
    }

    @Override
    public String getDescription() {
        return "Remove all project current user";
    }

    @Override
    public void execute() {
        @Nullable User currentUser = serviceLocator.getCurrentUser();
        if (currentUser != null) {
            @NotNull final String currentUserName = currentUser.getName();
            ConsoleHelperUtil.writeString("Удаление задач пользователя " + currentUserName);
            serviceLocator.getTaskService().removeAllTaskByUserId(currentUser.getId());
            ConsoleHelperUtil.writeString("Удаление проектов пользователя " + currentUserName);
            serviceLocator.getProjectService().removeAllByUserId(currentUser.getId());
            ConsoleHelperUtil.writeString("Удаление завершено");
        } else {
            ConsoleHelperUtil.writeString("Текущий пользователь не установлен");
        }
    }
}
