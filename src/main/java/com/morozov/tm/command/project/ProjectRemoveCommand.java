package com.morozov.tm.command.project;

import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.exception.StringEmptyException;
import com.morozov.tm.entity.User;
import com.morozov.tm.enumerated.UserRoleEnum;
import com.morozov.tm.util.ConsoleHelperUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class ProjectRemoveCommand extends AbstractCommand {
    public ProjectRemoveCommand() {
        userRoleList.add(UserRoleEnum.ADMIN);
        userRoleList.add(UserRoleEnum.USER);
    }

    @NotNull
    @Override
    final public String getName() {
        return "project-remove";
    }

    @NotNull
    @Override
    final public String getDescription() {
        return "Remove selected project";
    }

    @Override
    final public void execute() throws StringEmptyException {
        @Nullable final User currentUser = serviceLocator.getCurrentUser();
        ConsoleHelperUtil.writeString("Введите ID проекта для удаления");
        @NotNull final String idDeletedProject = ConsoleHelperUtil.readString();
        if (currentUser != null) {
            if (serviceLocator.getProjectService().removeProjectById(currentUser.getId(), idDeletedProject)) {
                ConsoleHelperUtil.writeString("Проект с ID: " + idDeletedProject + " удален");
                ConsoleHelperUtil.writeString("Удаление задач с ID проекта: " + idDeletedProject);
                serviceLocator.getTaskService().removeAllTaskByProjectId(currentUser.getId(), idDeletedProject);
            } else {
                ConsoleHelperUtil.writeString("Проекта с данным ID не существует");
            }
        } else {
            ConsoleHelperUtil.writeString("Текущий пользователь не установлен");
        }
    }
}
