package com.morozov.tm.command.task;

import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.entity.Task;
import com.morozov.tm.exception.StringEmptyException;
import com.morozov.tm.entity.User;
import com.morozov.tm.enumerated.UserRoleEnum;
import com.morozov.tm.util.ConsoleHelperUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class TaskCreateCommand extends AbstractCommand {

    public TaskCreateCommand() {
        userRoleList.add(UserRoleEnum.ADMIN);
        userRoleList.add(UserRoleEnum.USER);
    }

    @NotNull
    @Override
    final public String getName() {
        return "task-create";
    }

    @NotNull
    @Override
    final public String getDescription() {
        return "Create task";
    }

    @Override
    final public void execute() throws StringEmptyException {
        @Nullable final User currentUser = serviceLocator.getCurrentUser();
        ConsoleHelperUtil.writeString("Введите имя новой задачи");
        @NotNull final String taskName = ConsoleHelperUtil.readString();
        ConsoleHelperUtil.writeString("Введите ID проекта задачи");
        @NotNull final String projectId = ConsoleHelperUtil.readString();
        if (currentUser != null) {
            @NotNull final Task task = serviceLocator.getTaskService().addTask(currentUser.getId(), taskName, projectId);
            ConsoleHelperUtil.writeString("Добавлена задача " + task.getName());
        } else {
            ConsoleHelperUtil.writeString("Текущий пользователь не установлен");
        }
    }
}
