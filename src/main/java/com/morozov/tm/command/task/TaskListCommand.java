package com.morozov.tm.command.task;

import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.entity.Task;
import com.morozov.tm.exception.RepositoryEmptyException;
import com.morozov.tm.entity.User;
import com.morozov.tm.enumerated.UserRoleEnum;
import com.morozov.tm.util.ComparatorFactoryUtil;
import com.morozov.tm.util.ConsoleHelperUtil;
import com.morozov.tm.enumerated.CompareTypeUnum;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Arrays;
import java.util.List;

public class TaskListCommand extends AbstractCommand {

    public TaskListCommand() {
        userRoleList.add(UserRoleEnum.ADMIN);
        userRoleList.add(UserRoleEnum.USER);
    }

    @NotNull
    @Override
    final public String getName() {
        return "task-list";
    }

    @NotNull
    @Override
    final public String getDescription() {
        return "Show all tasks";
    }

    @Override
    final public void execute() throws RepositoryEmptyException {
        @Nullable final User currentUser = serviceLocator.getCurrentUser();
        if (currentUser != null) {
            @NotNull final List<Task> taskList = serviceLocator.getTaskService().getAllTaskByUserId(currentUser.getId());
            ConsoleHelperUtil.writeString("Список задач пользователя " + currentUser.getName());
            @NotNull final CompareTypeUnum compareTypeUnum = ConsoleHelperUtil.printSortedVariant();
            if (compareTypeUnum != null) {
                ConsoleHelperUtil.writeString("Сортировка по " + compareTypeUnum.getName());
                @NotNull final Task[] tasks = taskList.toArray(new Task[0]);
                Arrays.sort(tasks, ComparatorFactoryUtil.getComparator(compareTypeUnum));
                ConsoleHelperUtil.printTaskList(Arrays.asList(tasks));
            } else {
                ConsoleHelperUtil.writeString("Сортировка не выбрана.");
                ConsoleHelperUtil.printTaskList(taskList);
            }
        } else {
            ConsoleHelperUtil.writeString("Текущий пользователь не установлен");
        }
    }
}
