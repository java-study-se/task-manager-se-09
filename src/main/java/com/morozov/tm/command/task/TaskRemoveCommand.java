package com.morozov.tm.command.task;

import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.exception.StringEmptyException;
import com.morozov.tm.entity.User;
import com.morozov.tm.enumerated.UserRoleEnum;
import com.morozov.tm.util.ConsoleHelperUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class TaskRemoveCommand extends AbstractCommand {

    public TaskRemoveCommand() {
        userRoleList.add(UserRoleEnum.ADMIN);
        userRoleList.add(UserRoleEnum.USER);
    }

    @NotNull
    @Override
    final public String getName() {
        return "task-remove";
    }

    @NotNull
    @Override
    final public String getDescription() {
        return "Remove selected task";
    }

    @Override
    final public void execute() throws StringEmptyException{
        @Nullable final User currentUser = serviceLocator.getCurrentUser();
        ConsoleHelperUtil.writeString("Введите порядковый номер задачи для удаления");
        @NotNull final String idDeletedTask = ConsoleHelperUtil.readString();
        if (currentUser != null) {
            if (serviceLocator.getTaskService().removeTaskById(currentUser.getId(), idDeletedTask)) {
                ConsoleHelperUtil.writeString("Задача с порядковым номером " + idDeletedTask + " удален");
            }
            ConsoleHelperUtil.writeString("Задачи с данным ID не существует");
        } else {
            ConsoleHelperUtil.writeString("Текущий пользователь не установлен");
        }
    }
}
