package com.morozov.tm.command.user;

import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.exception.StringEmptyException;
import com.morozov.tm.exception.UserExistException;
import com.morozov.tm.util.ConsoleHelperUtil;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;

public class UserRegistryCommand extends AbstractCommand {
    @NotNull
    @Override
    final public String getName() {
        return "user-reg";
    }

    @NotNull
    @Override
    final public String getDescription() {
        return "Registry new user";
    }

    @Override
    final public void execute() throws UserExistException, StringEmptyException{
        ConsoleHelperUtil.writeString("Введите имя пользователя");
        @NotNull final String login = ConsoleHelperUtil.readString();
        ConsoleHelperUtil.writeString("Введите пароль пользователя");
        @NotNull final String password = ConsoleHelperUtil.readString();
        serviceLocator.getUserService().registryUser(login, password);
        ConsoleHelperUtil.writeString("Пользователь с логином " + login + " зарегистрирован");
    }
}
