package com.morozov.tm.service;

import com.morozov.tm.api.ITaskRepository;
import com.morozov.tm.entity.Task;
import com.morozov.tm.enumerated.StatusEnum;
import com.morozov.tm.exception.RepositoryEmptyException;
import com.morozov.tm.exception.StringEmptyException;
import com.morozov.tm.util.ConsoleHelperUtil;
import com.morozov.tm.util.DataValidationUtil;
import org.jetbrains.annotations.NotNull;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public final class TaskService implements com.morozov.tm.api.ITaskService {

    final private ITaskRepository taskRepository;

    public TaskService(@NotNull ITaskRepository TaskRepository) {
        this.taskRepository = TaskRepository;
    }

    @Override
    @NotNull
    public List<Task> getAllTaskByUserId(@NotNull final String userId) throws RepositoryEmptyException {
        @NotNull final List<Task> taskList = taskRepository.findAllByUserId(userId);
        DataValidationUtil.checkEmptyRepository(taskList);
        return taskList;
    }

    @Override
    @NotNull
    public Task addTask(@NotNull final String userId, @NotNull final String taskName, @NotNull final String projectId)
        throws StringEmptyException {
        DataValidationUtil.checkEmptyString(taskName, projectId);
        @NotNull final Task task = new Task();
        task.setName(taskName);
        task.setIdProject(projectId);
        task.setUserId(userId);
        taskRepository.persist(task.getId(), task);
        return task;
    }

    @Override
    public boolean removeTaskById(@NotNull final String userId, @NotNull final String id) throws StringEmptyException {
        boolean isDeleted = false;
        DataValidationUtil.checkEmptyString(id);
        if (taskRepository.findOneByUserId(userId, id) != null) {
            taskRepository.remove(id);
            isDeleted = true;
        }
        return isDeleted;
    }

    @Override
    public void updateTask(@NotNull final String userId, @NotNull final String id, @NotNull final String name,
        @NotNull final String description, @NotNull final String dataStart, @NotNull final String dataEnd,
        @NotNull final String projectId) throws RepositoryEmptyException, StringEmptyException, ParseException {
        DataValidationUtil.checkEmptyRepository(taskRepository.findAll());
        DataValidationUtil.checkEmptyString(id, name, description, projectId);
        @NotNull final Task task = (Task) taskRepository.findOne(id);
        task.setId(id);
        task.setName(name);
        task.setDescription(description);
        final Date updatedStartDate = ConsoleHelperUtil.formattedData(dataStart);
        if (updatedStartDate != null) {
            task.setStatus(StatusEnum.PROGRESS);
        }
        task.setStartDate(updatedStartDate);
        final Date updatedEndDate = ConsoleHelperUtil.formattedData(dataEnd);
        if (updatedEndDate != null) {
            task.setStatus(StatusEnum.READY);
        }
        task.setEndDate(updatedEndDate);
        task.setIdProject(projectId);
        task.setUserId(userId);
        taskRepository.merge(id, task);
    }

    @Override
    @NotNull
    public List<Task> getAllTaskByProjectId(@NotNull final String userId, @NotNull final String projectId)
        throws StringEmptyException, RepositoryEmptyException {
        DataValidationUtil.checkEmptyString(projectId);
        @NotNull final List<Task> resultTaskList = taskRepository.findAllByProjectIdUserId(userId, projectId);
        DataValidationUtil.checkEmptyRepository(resultTaskList);
        return resultTaskList;
    }

    @Override
    public void removeAllTaskByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        taskRepository.deleteAllTaskByProjectId(userId, projectId);
    }

    @Override
    public void clearTaskList() {
        taskRepository.removeAll();
    }

    @Override
    @NotNull
    public List<Task> findTaskByStringInNameOrDescription(@NotNull final String userId, @NotNull final String string)
        throws RepositoryEmptyException {
        if (string.isEmpty()) return taskRepository.findAllByUserId(userId);
        @NotNull final List<Task> taskListByProjectId = taskRepository.findTaskByStringInNameOrDescription(userId, string);
        DataValidationUtil.checkEmptyRepository(taskListByProjectId);
        return taskListByProjectId;
    }

    @Override
    public void removeAllTaskByUserId(@NotNull final String userId) {
        taskRepository.removeAllByUserId(userId);
    }
}
