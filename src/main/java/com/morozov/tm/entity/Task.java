package com.morozov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

@Getter
@Setter
@NoArgsConstructor
public class Task extends AbstractWorkEntity {
    @NotNull
    private String idProject = "";
}
