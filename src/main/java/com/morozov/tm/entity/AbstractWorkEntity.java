package com.morozov.tm.entity;

import com.morozov.tm.enumerated.StatusEnum;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractWorkEntity extends AbstractEntity {
    @NotNull
    private String userId = "";
    @NotNull
    private String description = "";
    @Nullable
    private Date startDate = null;
    @Nullable
    private Date endDate = null;
    @NotNull
    private StatusEnum status = StatusEnum.PLANNED;

}
