package com.morozov.tm.exception;

public class UserExistException extends Exception {
    public UserExistException() {
        super("Такой пользователь уже существует");
    }
}
