package com.morozov.tm.api;

import com.morozov.tm.command.AbstractCommand;
import com.morozov.tm.service.ProjectService;
import com.morozov.tm.service.TaskService;
import com.morozov.tm.entity.User;
import com.morozov.tm.service.UserService;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IServiceLocator {

    List<AbstractCommand> getCommandList();

    @NotNull
    IProjectService getProjectService();

    @NotNull
    ITaskService getTaskService();

    @NotNull
    IUserService getUserService();

    @Nullable
    User getCurrentUser();

    void setCurrentUser(@Nullable User user);
}
