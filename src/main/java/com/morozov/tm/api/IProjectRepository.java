package com.morozov.tm.api;


import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IProjectRepository<T> extends IRepository<T> {
    @NotNull
    List<T> findAllByUserId(@NotNull final String userId);

    @Nullable
    T findOneByUserId(@NotNull final String userId, @NotNull final String id);
    @NotNull
    List<T> findProjectByStringInNameOrDescription(@NotNull final String userId, @NotNull final String string);

    void removeAllByUserId(@NotNull final String userId);
}
