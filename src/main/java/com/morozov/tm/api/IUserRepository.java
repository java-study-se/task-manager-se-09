package com.morozov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface IUserRepository<T> extends IRepository<T> {
    @Nullable
    T findOneByLogin(@NotNull final String login);
}

