package com.morozov.tm.api;

import com.morozov.tm.entity.User;
import com.morozov.tm.exception.StringEmptyException;
import com.morozov.tm.exception.UserExistException;
import com.morozov.tm.exception.UserNotFoundException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface IUserService {
    @Nullable User loginUser(@NotNull String login, @NotNull String password) throws UserNotFoundException, StringEmptyException;

    void registryUser(@NotNull String login, @NotNull String password) throws UserExistException, StringEmptyException;

    void updateUserPassword(@NotNull String id, @NotNull String newPassword) throws StringEmptyException, UserNotFoundException;

    void updateUserProfile(@NotNull String id,
                           @NotNull String newUserName,
                           @NotNull String newUserPassword) throws StringEmptyException, UserExistException, UserNotFoundException;
}
