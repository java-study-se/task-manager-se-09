package com.morozov.tm.api;

import com.morozov.tm.entity.Project;
import com.morozov.tm.exception.RepositoryEmptyException;
import com.morozov.tm.exception.StringEmptyException;
import org.jetbrains.annotations.NotNull;

import java.text.ParseException;
import java.util.List;

public interface IProjectService {
    @NotNull List<Project> getAllProject() throws RepositoryEmptyException;

    @NotNull List<Project> getAllProjectByUserId(@NotNull String userId) throws RepositoryEmptyException;

    Project addProject(@NotNull String userId, @NotNull String projectName) throws StringEmptyException;

    boolean removeProjectById(@NotNull String userId, @NotNull String id) throws StringEmptyException;

    void updateProject(@NotNull String userId, @NotNull String id, @NotNull String projectName,
    @NotNull String projectDescription, @NotNull String dataStart, @NotNull String dataEnd)
    throws RepositoryEmptyException, StringEmptyException, ParseException;

    @NotNull List<Project> findProjectByStringInNameOrDescription(@NotNull String userId, @NotNull String string)
    throws RepositoryEmptyException;

    void removeAllByUserId(@NotNull String userId);

    void clearProjectList();
}
