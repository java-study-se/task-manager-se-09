package com.morozov.tm.repository;

import com.morozov.tm.api.ITaskRepository;
import com.morozov.tm.entity.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository<Task> {

    @NotNull
    @Override
    public List<Task> findAllByProjectIdUserId(@NotNull final String userId, @NotNull final String projectId) {
        @NotNull final List<Task> resultListByUserId = new ArrayList<>();
        for (@NotNull final Task task : getAllTaskByProjectId(projectId)) {
            if (task.getUserId().equals(userId)) resultListByUserId.add(task);
        }
        return resultListByUserId;
    }

    @NotNull
    @Override
    public List<Task> findAllByUserId(@NotNull final String userId) {
        final List<Task> resultTaskList = new ArrayList<>();
        for (@NotNull final Task task : findAll()) {
            if (task.getUserId().equals(userId)) resultTaskList.add(task);
        }
        return resultTaskList;
    }

    @Nullable
    @Override
    public Task findOneByUserId(@NotNull final String userId, @NotNull final String id) {
        Task resultTask = null;
        for (@NotNull final Task task : findAllByUserId(userId)) {
            if (task.getId().equals(id)) resultTask = task;
        }
        return resultTask;
    }

    @NotNull
    @Override
    public List<Task> getAllTaskByProjectId(@NotNull final String projectId) {
        @NotNull List<Task> resultList = new ArrayList<>();
        for (@NotNull final Task task : entityMap.values()) {
            if (task.getIdProject().equals(projectId)) resultList.add(task);
        }
        return resultList;
    }

    @Override
    public void deleteAllTaskByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        @NotNull final List<Task> taskToDelete = findAllByProjectIdUserId(userId, projectId);
        for (@NotNull final Task task : taskToDelete) {
            remove(task.getId());
        }
    }

    @Override
    public List<Task> findTaskByStringInNameOrDescription(@NotNull final String userId, @NotNull final String string) {
        @NotNull final List<Task> resultTaskList = new ArrayList<>();
        for (@NotNull final Task task : findAllByUserId(userId)) {
            if (task.getName().contains(string) || task.getDescription().contains(string)) resultTaskList.add(task);
        }
        return resultTaskList;
    }

    @Override
    public void removeAllByUserId(@NotNull final String userId) {
        @NotNull final List<Task> taskListForRemove = findAllByUserId(userId);
        for (Task task : taskListForRemove) {
            entityMap.remove(task.getId());
        }
    }
}
