package com.morozov.tm.repository;

import com.morozov.tm.api.IProjectRepository;
import com.morozov.tm.entity.Project;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository<Project> {
    @NotNull
    @Override
    public List<Project> findAllByUserId(@NotNull final String userId) {
        @NotNull final List<Project> projectListByUserID = new ArrayList<>();
        for (@NotNull final Project project : findAll()) {
            if (project.getUserId().equals(userId)) projectListByUserID.add(project);
        }
        return projectListByUserID;
    }

    @Nullable
    @Override
    public Project findOneByUserId(@NotNull final String userId, @NotNull final String id) {
        Project resultProject = null;
        for (@NotNull final Project project : findAllByUserId(userId)) {
            if (project.getId().equals(id)) resultProject = project;
        }
        return resultProject;
    }

    @NotNull
    @Override
    public List<Project> findProjectByStringInNameOrDescription(@NotNull final String userId, @NotNull final String string) {
        @NotNull final List<Project> resultProjectList = new ArrayList<>();
        for (@NotNull final Project project : findAllByUserId(userId)) {
            if (project.getName().contains(string) || project.getDescription().contains(string))
                resultProjectList.add(project);
        }
        return resultProjectList;
    }

    @Override
    public void removeAllByUserId(@NotNull final String userId) {
        @NotNull final List<Project> projectListForDelete = findAllByUserId(userId);
        for (@NotNull final Project project : projectListForDelete) {
            entityMap.remove(project.getId());
        }
    }
}
