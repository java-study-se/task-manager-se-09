package com.morozov.tm.repository;

import com.morozov.tm.api.IRepository;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class AbstractRepository<T> implements IRepository<T> {
    @NotNull
    final Map<String, T> entityMap = new HashMap<>();

    @NotNull
    @Override
    public List<T> findAll() {
        return new ArrayList<>(entityMap.values());
    }

    @Override
    @Nullable
    public T findOne(@NotNull String id) {
        return entityMap.get(id);
    }

    @Override
    public void merge(@NotNull String id, @NotNull T updateEntity) {
        entityMap.put(id, updateEntity);
    }

    @Override
    public void persist(@NotNull String id, @NotNull T writeEntity) {
        entityMap.put(id, writeEntity);
    }

    @Override
    public void remove(@NotNull String id) {
        entityMap.remove(id);
    }

    @Override
    public void removeAll() {
        entityMap.clear();
    }
}
